package fr.crossthink.training.business.repositories;

import fr.crossthink.training.Application;
import fr.crossthink.training.business.models.Airport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(locations="classpath:application-test.properties")
@Transactional
public class AirportRepositoryTest {

    private static final String CITY = "TEST";
    private static final String CITY_CODE = "TST";

    @Autowired
    private AirportRepository airportRepository;

    @Test
    public void shouldAddAirport() {
        Airport airport = new Airport();
        airport.setCity(CITY);
        airport.setCityCode(CITY_CODE);

        airportRepository.save(airport);

        Airport savedAirport = airportRepository.getOne(airport.getId());
        assertThat(savedAirport, is(not(nullValue())));
        assertThat(savedAirport.getCity(),  equalTo(CITY));
        assertThat(savedAirport.getCityCode(), equalTo(CITY_CODE));
    }

    @Test
    public void shouldGetAllAirports() {
        List<Airport> airportList = airportRepository.findAll();
        assertThat(airportList, hasSize(4));
    }
}
