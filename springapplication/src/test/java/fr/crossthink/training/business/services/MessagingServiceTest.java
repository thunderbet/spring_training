package fr.crossthink.training.business.services;

import fr.crossthink.training.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(locations="classpath:application-test.properties")
public class MessagingServiceTest {

    @Autowired
    private MessagingService messagingService;

    @Test
    public void shouldReturnGreetingsMessage() {
        assertThat(messagingService.getGreetingMessage()).isEqualTo("Yo mates, this is the test greetings!");
    }
}
