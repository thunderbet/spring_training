package fr.crossthink.training.api.controllers;

import fr.crossthink.training.Application;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

/**
 * This test is an integration test
 * It actually boots up the application, so the REST controllers are live
 * then it really call them and checks the results...
 * After that all is shutdown automatically.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
public class FirstControllerTest {

    // The API url for the testing environement
    private static final String API_ROOT = "http://localhost:8080";
    // The GET request endpoint
    private static final String GREETINGS = "/api/test/greetings";
    private static final String AUTHENTICATE = "/authenticate";
    private static final String AIRPORTS = "/api/flightdata/airports";

    private static String token;

    @Test
    public void shouldAskToken() throws ParseException {
        JSONParser parser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);
        JSONObject requestParams = new JSONObject();
        requestParams.put("username", "crossthink");
        requestParams.put("password", "password");

//        RestAssured.given().auth()
//                .basic("mihai", "password")
//                .when()
//                .get(API_ROOT + GREETINGS)
//                .then()
//                .assertThat()
//                .statusCode(HttpStatus.OK.value());

        // Executing the GET request
        RequestSpecification request = RestAssured.given();
        // Add a header stating the Request body is a JSON
        request.header("Content-Type", "application/json");

        // Add the Json to the body of the request
        request.body(requestParams.toJSONString());

        // Post the request and check the response
        Response response = request.post(API_ROOT + AUTHENTICATE);

        // Checking that the HTTP code is 200 and that the content is "Yo mates, this is the test greetings!" (test greetings)
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        JSONObject tokenJson = (JSONObject) parser.parse(response.asString());
        token = tokenJson.getAsString("token");
        assertTrue(token != null);
    }

    @Test
    public void shouldCallGreetings() {
        RequestSpecification request = RestAssured.given();
        // Add a header stating the Request body is a JSON
        request.header("Content-Type", "application/json");
        request.header("Authorization", "Bearer " + token);

        // Executing the GET request
        Response response = request.get(API_ROOT + GREETINGS);

        // Checking that the HTTP code is 200 and that the content is "Yo mates, this is the test greetings!" (test greetings)
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(response.asString().equals("Yo mates, this is the test greetings!"));
    }

    @Test
    public void shouldGetAllAirports() throws ParseException {
        JSONParser parser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);
        RequestSpecification request = RestAssured.given();
        // Add a header stating the Request body is a JSON
        request.header("Content-Type", "application/json");
        request.header("Authorization", "Bearer " + token);

        // Executing the GET request
        Response response = request.get(API_ROOT + AIRPORTS);

        // Checking that the HTTP code is 200 and that the content is "Yo mates, this is the test greetings!" (test greetings)
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        JSONArray tokenJson = (JSONArray) parser.parse(response.asString());
        assertThat(tokenJson, is(not(nullValue())));
        assertThat(tokenJson, hasSize(4));
    }
}
