package fr.crossthink.training.batch.csv;

import fr.crossthink.training.business.models.Airport;
import org.springframework.batch.item.ItemWriter;
import java.util.List;

public class AirportItemWriter implements ItemWriter<Airport> {

    @Override
    public void write(List<? extends Airport> list) throws Exception {
        for (Airport airport : list) {
//            System.out.println(airport);
        }
    }
}
