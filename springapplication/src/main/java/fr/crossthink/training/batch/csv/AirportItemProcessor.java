package fr.crossthink.training.batch.csv;

import fr.crossthink.training.business.models.Airport;
import org.springframework.batch.item.ItemProcessor;

public class AirportItemProcessor implements ItemProcessor<Airport, Airport> {
    @Override
    public Airport process(Airport airport) throws Exception {
        return airport;
    }
}
