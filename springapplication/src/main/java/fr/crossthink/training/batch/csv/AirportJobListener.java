package fr.crossthink.training.batch.csv;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class AirportJobListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("Airport BEFORE job");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("Airport AFTER job");
    }
}
