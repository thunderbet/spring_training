package fr.crossthink.training.business.repositories;

import fr.crossthink.training.business.models.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Flight repository
 */
public interface FlightRepository extends JpaRepository<Flight, Long> {

    /**
     * Returns the flight with the given id
     * @param id
     * @return
     */
    Flight findById(long id);

    /**
     * Returns the flights whose departure cities matches the given string, case ignored
     * @param city
     * @return
     */
    List<Flight> findByDepartureAirportCityIgnoreCase(String city);
}
