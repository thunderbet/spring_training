package fr.crossthink.training.business.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "FLIGHT",
        indexes = {
                @Index(
                        name = "IDX_FLIGHT_NUMBER",
                        columnList = "FLIGHT_NUMBER",
                        unique = false
                )
        }
)
public class Flight {

    @Id
    @Column(name="FLIGHT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "FLIGHT_NUMBER", nullable = false, unique = true)
    private String flightNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTURE_AIRPORT_ID")
    private Airport departureAirport;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ARRIVAL_AIRPORT_ID")
    private Airport arrivalAirport;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }


    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }
}
