package fr.crossthink.training.business.services;

import fr.crossthink.training.business.dto.AirportDTO;
import fr.crossthink.training.commons.events.AirportCreatedEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class MessagingService {

    @Value("${my.hello.message}")
    private String helloMessage;

    public String getGreetingMessage() {
        return helloMessage;
    }

    @EventListener
    public void printMessageWhenAirportCreadted(AirportCreatedEvent airportCreatedEvent) {
        AirportDTO airportDTO = (AirportDTO) airportCreatedEvent.getSource();
        System.out.println("Created airport " + airportDTO.getCity() + " with code " + airportDTO.getCityCode());
    }
}
