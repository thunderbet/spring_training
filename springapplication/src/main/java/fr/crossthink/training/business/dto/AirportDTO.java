package fr.crossthink.training.business.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;


public class AirportDTO {


    @NotNull(message = "city should not be null")
    private String city;

    @NotNull(message = "city code should not be null")
    private String cityCode;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirportDTO that = (AirportDTO) o;
        return city.equals(that.city) &&
                cityCode.equals(that.cityCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, cityCode);
    }
}
