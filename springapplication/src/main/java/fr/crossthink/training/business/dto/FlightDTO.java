package fr.crossthink.training.business.dto;

import java.util.Objects;

/**
 * DTO for the Flight entity
 */
public class FlightDTO {

    private String flightNumber;

    private AirportDTO departureAirport;

    private AirportDTO arrivalAirport;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirportDTO getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(AirportDTO departureAirport) {
        this.departureAirport = departureAirport;
    }

    public AirportDTO getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(AirportDTO arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightDTO flightDTO = (FlightDTO) o;
        return Objects.equals(flightNumber, flightDTO.flightNumber) &&
                Objects.equals(departureAirport, flightDTO.departureAirport) &&
                Objects.equals(arrivalAirport, flightDTO.arrivalAirport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightNumber, departureAirport, arrivalAirport);
    }
}
