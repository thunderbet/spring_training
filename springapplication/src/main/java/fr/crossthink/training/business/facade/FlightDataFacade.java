package fr.crossthink.training.business.facade;

import fr.crossthink.training.api.models.AirportViewModel;
import fr.crossthink.training.api.models.FlightViewModel;
import fr.crossthink.training.business.dto.AirportDTO;
import fr.crossthink.training.business.dto.FlightDTO;
import fr.crossthink.training.business.models.Airport;
import fr.crossthink.training.business.models.Flight;
import fr.crossthink.training.business.repositories.AirportRepository;
import fr.crossthink.training.business.repositories.FlightRepository;
import fr.crossthink.training.exceptions.InvalidIdException;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Business facade centralising all the flight related repositories and business logic
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
public class FlightDataFacade {

    // Repositories
    private FlightRepository flightRepository;
    private AirportRepository airportRepository;
    KafkaTemplate<String, String> kafkaTemplate;

    // Utils
    private ModelMapper modelMapper;

    public FlightDataFacade(AirportRepository airportRepository, FlightRepository flightRepository, KafkaTemplate<String, String> kafkaTemplate) {
        this.airportRepository = airportRepository;
        this.flightRepository = flightRepository;
        this.modelMapper = new ModelMapper();
        this.kafkaTemplate = kafkaTemplate;
    }


    /*
     *  AIPORT CRUD
     */

    public List<AirportViewModel> getAllAirports() {
        List<Airport> airportList = airportRepository.findAll();

        return airportList.stream().map(airport -> modelMapper.map(airport, AirportViewModel.class)).collect(Collectors.toList());
    }

    public AirportViewModel findAirportById(Long id) {
        Airport airport = airportRepository.getOne(id);
        return modelMapper.map(airport, AirportViewModel.class);
    }

    public List<Airport> getAllAirportsDto() {
        return airportRepository.findAll();
    }

    public List<Airport> getAirportsByCity(String city) {
        return airportRepository.findByCityStartingWithIgnoreCase(city);
    }

    public AirportDTO saveAirport(AirportDTO airportDTO) {
        Airport airport = airportRepository.save(modelMapper.map(airportDTO, Airport.class));
        kafkaTemplate.send("spring", "New airport created : " + airportDTO.getCity());
        return modelMapper.map(airport, AirportDTO.class);
    }

    public void deleteAirport(long id) {
        airportRepository.deleteById(id);
    }


    public AirportDTO updateAirport(AirportDTO airportDTO, long id) {
        Airport airport = airportRepository.findById(id);

        if (airport == null) {
            throw new InvalidIdException("Airport with id " + id + " doesn't exist");
        }

        modelMapper.map(airportDTO, airport);

        airportRepository.save(airport);

        return airportDTO;
    }



    /*
     *  FLIGHT CRUD
     */
    public List<FlightViewModel> getAllFlights() {
        List<Flight> flightList = flightRepository.findAll();

        return  flightList.stream().map(flight -> modelMapper.map(flight, FlightViewModel.class)).collect(Collectors.toList());
    }

    public List<Flight> getAllFlightEntities() {
        return flightRepository.findAll();
    }

    public List<FlightDTO> getFlightsByDepartyreCity(String departureCity) {
        List<Flight> flightList = flightRepository.findByDepartureAirportCityIgnoreCase(departureCity);

        return  flightList.stream().map(flight -> modelMapper.map(flight, FlightDTO.class)).collect(Collectors.toList());
    }

    public FlightDTO saveFlightModel(FlightViewModel flightViewModel) {
        Flight flight = modelMapper.map(flightViewModel, Flight.class);
        flight.setDepartureAirport(airportRepository.getOne(flightViewModel.getDepartureAirport().getId()));
        flight.setArrivalAirport(airportRepository.getOne(flightViewModel.getArrivalAirport().getId()));

        return modelMapper.map(flightRepository.save(flight), FlightDTO.class);
    }

    public void deleteFlight(long id) {
        flightRepository.deleteById(id);
    }
}
