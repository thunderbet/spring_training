package fr.crossthink.training.business.repositories;

import fr.crossthink.training.business.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Airport repository
 */
public interface AirportRepository extends JpaRepository<Airport, Long> {

    /**
     * Returns the flight with the given id
     * @param id
     * @return
     */
    Airport findById(long id);

    /**
     * Returns the airports where the city starts with the given string, case ignored
     * @param city
     * @return
     */
    List<Airport> findByCityStartingWithIgnoreCase(String city);
}
