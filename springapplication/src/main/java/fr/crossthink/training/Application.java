package fr.crossthink.training;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Main class for launching the application.
 * <p>
 * The @SpringBootApplication contains the following :
 *
 * @SpringBootConfiguration
 * @EnableAutoConfiguration
 * @ComponentScan <p>
 * <p>
 * The @SpringBootApplication will detect all the components, entities, repositories, services
 * as long as they are in the same package or the sub packages.
 * <p>
 * So no need for @ComponentScan, @EnableJpaRepositories, etc...
 * The @EnableAutoConfiguration does it for you.
 */
@SpringBootApplication
@EnableScheduling
@EnableCaching
public class Application {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

//    @Bean
//    public CommandLineRunner runPreprocessRunner() {
//        return args -> {
//            JobParameters params = new JobParametersBuilder()
//                    .addString("paramTest", String.valueOf(System.currentTimeMillis()))
//                    .toJobParameters();
//            jobLauncher.run(job, params);
//        };
//    }

//    @Scheduled(cron = "* * * * * ?")
//    public void perform() throws Exception
//    {
//        JobParameters params = new JobParametersBuilder()
//                .addString("readCSVFileJob", String.valueOf(System.currentTimeMillis()))
//                .toJobParameters();
//        jobLauncher.run(job, params);
//    }
}
