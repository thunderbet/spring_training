package fr.crossthink.training.commons.events;

import fr.crossthink.training.business.dto.AirportDTO;
import org.springframework.context.ApplicationEvent;

public class AirportCreatedEvent extends ApplicationEvent {

    public AirportCreatedEvent(AirportDTO airport) {
        super(airport);
    }
}
