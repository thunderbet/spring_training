package fr.crossthink.training.api.controllers.rest;

import fr.crossthink.training.business.services.MessagingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/test")
public class FirstController {

    private MessagingService messagingService;

    public FirstController(MessagingService messagingService) {
        this.messagingService = messagingService;
    }

    @GetMapping("/greetings")
    public String getGreetingsMessage() {
        return messagingService.getGreetingMessage();
    }

    @GetMapping("/error")
    public ResponseEntity getTest() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error");
    }
}
