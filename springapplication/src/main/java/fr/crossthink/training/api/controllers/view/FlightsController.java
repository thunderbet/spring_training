package fr.crossthink.training.api.controllers.view;

import fr.crossthink.training.api.models.AirportViewModel;
import fr.crossthink.training.api.models.FlightViewModel;
import fr.crossthink.training.business.facade.FlightDataFacade;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class FlightsController {

    private FlightDataFacade flightDataFacade;
    private ModelMapper modelMapper;

    public FlightsController(FlightDataFacade flightDataFacade) {
        this.flightDataFacade = flightDataFacade;
        this.modelMapper = new ModelMapper();
    }

    @GetMapping("/flights")
    public String flightPage(Model model) {
        List<FlightViewModel> flightViewModels = flightDataFacade.getAllFlights();
        List<AirportViewModel> airportViewModels = flightDataFacade.getAllAirports();

        model.addAttribute("flights", flightViewModels);
        model.addAttribute("airports", airportViewModels);
        model.addAttribute("newFlight", new FlightViewModel());

        return "flights";
    }

    @PostMapping("/flights/create")
    public String createFlight(@Valid @ModelAttribute FlightViewModel flightViewModel, BindingResult bindingResult) {
        if (bindingResult.getErrorCount() == 0 && flightViewModel != null) {
            AirportViewModel depatureAirport = flightDataFacade.findAirportById(flightViewModel.getDepartureAirport().getId());
            AirportViewModel arrivalAirport = flightDataFacade.findAirportById(flightViewModel.getArrivalAirport().getId());

            flightViewModel.setDepartureAirport(depatureAirport);
            flightViewModel.setArrivalAirport(arrivalAirport);

            flightDataFacade.saveFlightModel(flightViewModel);
        }

        return "redirect:/flights";
    }

    @PostMapping("/flights/delete/{id}")
    public String deleteFlight(@PathVariable Long id) {
        if (id != null) {
            flightDataFacade.deleteFlight(id);
        }

        return "redirect:/flights";
    }
}
