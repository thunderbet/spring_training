package fr.crossthink.training.api.models;

import fr.crossthink.training.business.models.Airport;

public class FlightViewModel {

    private long id;

    private String flightNumber;

    private AirportViewModel departureAirport;

    private AirportViewModel arrivalAirport;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirportViewModel getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(AirportViewModel departureAirport) {
        this.departureAirport = departureAirport;
    }

    public AirportViewModel getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(AirportViewModel arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }
}
