package fr.crossthink.training.api.controllers.rest;

import fr.crossthink.training.business.dto.AirportDTO;
import fr.crossthink.training.business.dto.FlightDTO;
import fr.crossthink.training.business.facade.FlightDataFacade;
import fr.crossthink.training.business.models.Airport;
import fr.crossthink.training.business.models.Flight;
import fr.crossthink.training.commons.events.AirportCreatedEvent;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for all the flight data
 */
@RestController
@RequestMapping("/api/flightdata")
public class FlightController {

    private FlightDataFacade flightDataFacade;
    private ApplicationEventPublisher applicationEventPublisher;

    public FlightController(FlightDataFacade flightDataFacade,  ApplicationEventPublisher applicationEventPublisher) {
        this.flightDataFacade = flightDataFacade;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Cacheable(value = "airports")
    @GetMapping("/airports")
    public List<Airport> getAllAirports() {
        return flightDataFacade.getAllAirportsDto();
    }

    @Cacheable(value = "airports", key="#city")
    @GetMapping("/airport/{city}")
    public List<Airport> getAirportsByCity(@PathVariable String city) {
        return flightDataFacade.getAirportsByCity(city);
    }

    @CacheEvict(value = "airports", allEntries = true)
    @PostMapping("/airport")
    @ResponseStatus(HttpStatus.CREATED)
    public AirportDTO create(@Valid @RequestBody AirportDTO airportDTO) {
        AirportDTO airportCreated = flightDataFacade.saveAirport(airportDTO);

        applicationEventPublisher.publishEvent(new AirportCreatedEvent(airportCreated));

        return airportCreated;
    }

    @CacheEvict(value = "airports", allEntries = true)
    @DeleteMapping("/airport/{id}")
    public void deleteAirport(@PathVariable Long id) {
        flightDataFacade.deleteAirport(id);
    }

    @Cacheable(value = "airports", key="#city", unless = "#city == null")
    @PutMapping("/airport/{airportId}")
    public AirportDTO updateAirport(@RequestBody AirportDTO airportDTO, @PathVariable Long airportId) {
        return flightDataFacade.updateAirport(airportDTO, airportId);
    }

    @GetMapping("/flight/{departureCity}")
    public List<FlightDTO> getFlightsByDepartyreCity(@PathVariable String departureCity) {
        List<FlightDTO> flightList = flightDataFacade.getFlightsByDepartyreCity(departureCity);
        return flightList;
    }

    @GetMapping("/flight/all")
    public List<Flight> getFlights() {
        return flightDataFacade.getAllFlightEntities();
    }
}
