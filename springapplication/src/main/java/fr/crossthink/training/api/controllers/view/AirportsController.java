package fr.crossthink.training.api.controllers.view;

import fr.crossthink.training.api.models.AirportViewModel;
import fr.crossthink.training.business.dto.AirportDTO;
import fr.crossthink.training.business.facade.FlightDataFacade;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AirportsController {

    private FlightDataFacade flightDataFacade;
    private ModelMapper modelMapper;

    public AirportsController(FlightDataFacade flightDataFacade) {
        this.flightDataFacade = flightDataFacade;
        this.modelMapper = new ModelMapper();
    }

    @GetMapping("/airports")
    public String airportHome(Model model) {

        List<AirportViewModel> airports = flightDataFacade.getAllAirports();
        model.addAttribute("airports", airports);
        model.addAttribute("newAirport", new AirportViewModel());

        return "airports";
    }

    @PostMapping("/airports/create")
    public String createAirport(@Valid @ModelAttribute AirportViewModel airportViewModel, BindingResult bindingResult) {
        if (bindingResult.getErrorCount() == 0 && airportViewModel != null) {
            AirportDTO airportDTO = new AirportDTO();
            modelMapper.map(airportViewModel, airportDTO);
            flightDataFacade.saveAirport(airportDTO);
        }

        return "redirect:/airports";
    }

    @PostMapping("/airports/delete/{id}")
    public String deleteAirport(@PathVariable Long id) {
        if (id != null) {
            flightDataFacade.deleteAirport(id);
        }

        return "redirect:/airports";
    }
}
